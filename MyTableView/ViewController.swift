//
//  File.swift
//  NyTest1
//
//  Created by Howard on 2021/7/14.
//

import UIKit

class ViewController: UIViewController {
    
    private lazy var finishBut = { () -> UIBarButtonItem in
        let but = UIBarButtonItem(title: "完成", style: .done, target: self, action: #selector(ViewController.editButton))
        return but
    }()
    
    private lazy var editBut = { () -> UIBarButtonItem in
        let but =  UIBarButtonItem(title: "编辑", style: .done, target: self, action: #selector(ViewController.editButton))
        return but
    }()
    
    private var myTableView: MyTableView!
    
    private let fullScreenSize = UIScreen.main.bounds.size
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.navigationController!.navigationBar.isTranslucent = false
        navigationItem.leftBarButtonItem = editBut
        myTableView = MyTableView(frame: CGRect(x: 0, y: 0, width: fullScreenSize.width, height: fullScreenSize.height), style: .grouped)
        myTableView.setEditing(true, animated: true)
        myTableView.setData(data: DataLoad.load())
        
        self.view.addSubview(myTableView)
        
        editBtnAction()
    }
    
    @objc func editBtnAction() {
        myTableView.setEditing(!myTableView.isEditing, animated: true)
        if (!myTableView.isEditing) {
            // 顯示編輯按鈕
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(ViewController.editBtnAction))

            // 顯示新增按鈕
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(ViewController.addBtnAction))
        } else {
            // 顯示編輯完成按鈕
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(ViewController.editBtnAction))
            
            // 隱藏新增按鈕
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    
    @objc func addBtnAction() {
        myTableView.addData("new value")
    }
}
// MARK: - 钩子
extension ViewController {
    
    @objc func editButton() {
        myTableView.setEditing(!myTableView.isEditing, animated: true)
        if myTableView.isEditing {
            navigationItem.leftBarButtonItem = finishBut
        } else {
            navigationItem.leftBarButtonItem = editBut
        }
    }
}
enum MyError: Error {
    case USEERROR
}
