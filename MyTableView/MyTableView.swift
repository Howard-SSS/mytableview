//
//  MyTableView.swift
//  MyTableView
//
//  Created by Howard on 2021/7/18.
//

import Foundation
import UIKit

class MyTableView: UITableView {
    
    struct TableGound {
        var name: String
        var arr: [String]
    }
    
    private var data: [TableGound] = []

    public override init(frame: CGRect, style: UITableView.Style) {
        
        super.init(frame: frame, style: style)
        
        super.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        // 默认多选
        self.allowsMultipleSelection = true
        
        delegate = self
        
        dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension MyTableView {
    // 获取某组数据
    func getData(section: Int) -> [String]{
        return data[section].arr
    }
    // 获取某项数据
    func getData(section: Int, row: Int) -> String {
        return data[section].arr[row]
    }
    // 获取全部数据
    func getData() -> [TableGound]{
        return self.data
    }
    // 设置数组内容
    func setData(data: [TableGound]) {
        self.data = data
    }
    // 添加数据
    func addData(_ value: String, section: Int = 0, row: Int = 0) {
        data[section].arr.insert(value, at: row)
        // 自动更新界面
        self.beginUpdates()
        self.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
        self.endUpdates()
    }
    // MARK: - 数据移动实现
    func moveCell(from sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let sourceSection = sourceIndexPath.section
        let destinationSection = destinationIndexPath.section
        let sourceValue = data[sourceSection].arr[sourceIndexPath.row]
        if sourceSection == destinationSection && sourceIndexPath.row == destinationIndexPath.row {
        }else{
            data[sourceSection].arr.remove(at: sourceIndexPath.row)
            data[destinationSection].arr.insert(sourceValue, at: destinationIndexPath.row)
        }
    }
}
// MARK: - 委托
extension MyTableView: UITableViewDelegate {
    
    // MARK: - 右滑
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let noRead = UIContextualAction(style: .normal, title: "标记未读") { (action, view, completionHandler) in
            
            completionHandler(true)
        }
        noRead.backgroundColor = .orange
        
        let configuration = UISwipeActionsConfiguration(actions: [noRead])
        configuration.performsFirstActionWithFullSwipe = false
        return configuration
    }
    // MARK: - 左滑
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let toTop = UIContextualAction(style: .normal, title: "置顶") { (action, view, completionHandler) in
            let to = IndexPath(row: 0, section: 0)
            self.moveCell(from: indexPath, to: to)
            tableView.moveRow(at: indexPath, to: to)
            completionHandler(true)
        }
        toTop.backgroundColor = .blue
        
        let del = UIContextualAction(style: .destructive, title: "删除") { (action, view, completionHandler) in
            completionHandler(true)
        }
        let configuration = UISwipeActionsConfiguration(actions: [toTop, del])
        configuration.performsFirstActionWithFullSwipe = false
        return configuration
    }
}
extension MyTableView: UITableViewDataSource {
    // MAKR: - 移动
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        moveCell(from: sourceIndexPath, to: destinationIndexPath)
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        if tableView.isEditing {
            // 多选
            let val:Int = UITableViewCell.EditingStyle.delete.rawValue | UITableViewCell.EditingStyle.insert.rawValue
            return UITableViewCell.EditingStyle.init(rawValue: val)!
        } else {
            // 侧滑
            return .delete
        }
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // 删除数据源一行
            self.data[indexPath.section].arr.remove(at: indexPath.row)
            // 删除界面一行
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade)
        }
    }
    // MARK: - 组标题
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return data[section].name
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    // MARK: - 设置每组多少单元
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data[section].arr.count
    }
    // MARK: - 单元定制
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        
        cell.accessoryType = .none
        // 显示内容
        if let myLabel = cell.textLabel {
            myLabel.text = "\(data[indexPath.section].arr[indexPath.row])"
        }

        return cell
    }
    // MARK: - 设置多少组
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
}
