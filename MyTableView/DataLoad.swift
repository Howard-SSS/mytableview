//
//  DataLoad.swift
//  MyTableView
//
//  Created by Howard on 2021/7/28.
//

import Foundation
import UIKit

class DataLoad {
    static func load() -> [MyTableView.TableGound]{
        var ret = [MyTableView.TableGound]()
        ret.append(MyTableView.TableGound(name: "薯片", arr: ["乐事","美的"]))
        ret.append(MyTableView.TableGound(name: "冰箱", arr: ["mac","windows"]))
        return ret
    }
}
